ruby-inotify (0.0.2-8) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 05:43:35 +0530

ruby-inotify (0.0.2-7) unstable; urgency=medium

  [ Cédric Boutillier ]
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL for Format field

  [ Christian Hofstaedtler ]
  * Remove transitional packages
  * Bump Standards-Version to 3.9.5 (no changes)
  * Update Build-Depends for ruby2.0, drop ruby1.8

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 01 Jan 2014 19:32:34 +0100

ruby-inotify (0.0.2-6) unstable; urgency=low

  * Source packages adapted according to the new Ruby policy:
    - Build for both ruby1.8 and ruby1.9.1.
    - Migrated to pkg-ruby-extras git repos. Changed the Vcs-* fields in
      debian/control accordingly.
    - Changed the depends and recommends to follow the new Ruby
      library naming scheme.
  * debian/control:
    - Added a default DM-Upload-Allowed field set to yes.
    - Standards-Version bumped to 3.9.2; no changes required.
    - Updated the homepage.
    - Set XS-Ruby-Versions to all.
    - Changed the build-depends for using gem2deb instead of ruby-pkg-tools.
    - Added rake as a build-depend.
    - Switched the maintainer with the uploaders field as per new
      convention the team is the default maintainer.
  * debian/copyright: reworked to fit the DEP5 format.

 -- Paul van Tilburg <paulvt@debian.org>  Sun, 25 Sep 2011 21:11:21 +0200

libinotify-ruby (0.0.2-5) unstable; urgency=low

  * Team upload.
  * Move to debian source format 3.0 (quilt)
  * Change require statements using relative paths for Ruby 1.9.2
    (Closes: #593033)
  * Bump Standards-Version to 3.9.1 (no changes).

 -- Laurent Arnoud <laurent@spkdev.net>  Fri, 24 Sep 2010 22:40:11 +0200

libinotify-ruby (0.0.2-4) unstable; urgency=low

  * Fixed Vcs-* fields after pkg-ruby-extras SVN layout change.
  * Drop package for Ruby 1.9, add package for ruby 1.9.1. Closes: #565827.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 19 Jan 2010 20:53:22 +1300

libinotify-ruby (0.0.2-3) unstable; urgency=low

  * Adopt package (Closes: #515303)
    - Update Maintainer, Uploaders (for pkg-ruby-extras team), and Vcs-*
  * remove get-orig-source target
  * change section to ruby
  * remove orig-tar.sh script
  * update to standards version 3.8.1
  * Debian copyright
  * add misc:Depends
  * extend long description a bit
  * extend short short descriptions a bit

 -- Ryan Niebur <ryanryan52@gmail.com>  Mon, 13 Apr 2009 22:01:03 -0700

libinotify-ruby (0.0.2-2) unstable; urgency=low

  * Remove debian/control.in because it is not really needed.
  * Set Architecture: any for the version specific binary packages.
    (Closes: #463816)
  * Update Homepage and Vcs headers in debian/control.
  * Update Standards-Version: 3.7.3 (no changes).
  * Add copyright information.

 -- Torsten Werner <twerner@debian.org>  Fri, 29 Feb 2008 22:28:31 +0100

libinotify-ruby (0.0.2-1) unstable; urgency=low

  * new upstream version
  * Remove patch build.diff that has been applied upstream.

 -- Torsten Werner <twerner@debian.org>  Wed, 10 Oct 2007 20:01:42 +0200

libinotify-ruby (0.0.1-2) unstable; urgency=low

  * Add a patch to build with newest ruby 1.9. Thanks to Akira Yamada.
    (Closes: #441489)

 -- Torsten Werner <twerner@debian.org>  Tue, 18 Sep 2007 22:41:13 +0200

libinotify-ruby (0.0.1-1) unstable; urgency=low

  [ Varun Hiremath ]
  * New usptream release
  * Remove debian/patches - all merged upstream
  * Enhance debian/watch
  * Add myself to Uploaders in debian/control

  [ Torsten Werner ]
  * Change Build-Depends: ruby(-dev) (>= 1.9.0+20070523).

 -- Torsten Werner <twerner@debian.org>  Mon, 11 Jun 2007 00:26:09 +0200

libinotify-ruby (0.0.0-3) unstable; urgency=low

  * Add patch ruby19.diff from Michael Ablassmeier. (Closes: #427481)

 -- Torsten Werner <twerner@debian.org>  Mon,  4 Jun 2007 20:37:53 +0200

libinotify-ruby (0.0.0-2) unstable; urgency=low

  * Add XS-X-Vcs-Svn header to debian/control.in.
  * Really fix the build process.

 -- Torsten Werner <twerner@debian.org>  Sun, 18 Mar 2007 21:04:16 +0100

libinotify-ruby (0.0.0-1) unstable; urgency=low

  * Initial release. (Closes: #415331)
  * Make sure the package build on every linux architecture.

 -- Torsten Werner <twerner@debian.org>  Sun, 18 Mar 2007 14:28:53 +0100
